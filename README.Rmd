---
output: rmarkdown::github_document
editor_options: 
  chunk_output_type: console
---
```{r pkg-knitr-opts, include=FALSE}
hrbrpkghelpr::global_opts()
```

```{r badges, results='asis', echo=FALSE, cache=FALSE}
hrbrpkghelpr::stinking_badges()
```

```{r description, results='asis', echo=FALSE, cache=FALSE}
hrbrpkghelpr::yank_title_and_description()
```

## What's Inside The Tin

The following functions are implemented:

```{r ingredients, results='asis', echo=FALSE, cache=FALSE}
hrbrpkghelpr::describe_ingredients()
```

## Installation

```{r install-ex, results='asis', echo=FALSE, cache=FALSE}
hrbrpkghelpr::install_block()
```

## Usage

```{r lib-ex}
library(checkdmarc)

# current version
packageVersion("checkdmarc")

```

```{r u01}
library(psl)
library(stringi)
library(tibble)

dom <- stri_trans_tolower("fbi.gov")
apex <- apex_domain(dom)

(ns_recs <- get_nameservers(dom))

(mx_recs <- get_mx_servers(dom, ns_recs$address))

(txt_recs <- get_txt_records(dom, ns_recs$address))

(spf_recs <- get_spf_records(dom, ns_recs$address))

(dmarc_rec <- get_dmarc_record(dom))

(get_bimi_record("ebay.com"))

(get_ptr_records("24.61.106.4"))
```

```{r u02, eval=FALSE}
(res <- check_starrtls(mx_recs$exchange[1]))
## [1] TRUE
```

## checkdmarc Metrics

```{r cloc, echo=FALSE}
cloc::cloc_pkg_md()
```

## Code of Conduct

Please note that this project is released with a Contributor Code of Conduct. 
By participating in this project you agree to abide by its terms.
