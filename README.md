
[![Project Status: Active – The project has reached a stable, usable
state and is being actively
developed.](https://www.repostatus.org/badges/latest/active.svg)](https://www.repostatus.org/#active)
[![Signed
by](https://img.shields.io/badge/Keybase-Verified-brightgreen.svg)](https://keybase.io/hrbrmstr)
![Signed commit
%](https://img.shields.io/badge/Signed_Commits-100%25-lightgrey.svg)
[![Linux build
Status](https://travis-ci.org/hrbrmstr/checkdmarc.svg?branch=master)](https://travis-ci.org/hrbrmstr/checkdmarc)  
![Minimal R
Version](https://img.shields.io/badge/R%3E%3D-3.3.0-blue.svg)
![License](https://img.shields.io/badge/License-MIT-blue.svg)

# checkdmarc

Gather and Validate SPF, DMARC, an BIMI DNS Records

## Description

Tools are provided to query for and check the validity of SPF, DMARC,
and BIMI DNS records for a domain.

## What’s Inside The Tin

The following functions are implemented:

  - `check_starrtls`: Check if a mail server supports STARTTLS
  - `checkdmarc_default_nameservers`: The nameservers the package
    defaults to using
  - `get_a_records`: Query DNS for A & AAAA records for an FQDN
  - `get_bimi_record`: Queries DNS for a BIMI record for a given FQDN
  - `get_dmarc_record`: Queries DNS for a DMARC record for a given FQDN
  - `get_mx_servers`: Query DNS for mail exchange records for an FQDN
  - `get_nameservers`: Query DNS for the nameservers for an FQDN
  - `get_ptr_records`: Query DNS for PTR records for an IP address
  - `get_spf_records`: Queries DNS for SPF recorsds for a given FQDN
  - `get_txt_records`: Query DNS for TXT records for an FQDN

## Installation

``` r
remotes::install_git("https://git.rud.is/hrbrmstr/checkdmarc.git")
# or
remotes::install_git("https://git.sr.ht/~hrbrmstr/checkdmarc")
# or
remotes::install_gitlab("hrbrmstr/checkdmarc")
# or
remotes::install_bitbucket("hrbrmstr/checkdmarc")
```

NOTE: To use the ‘remotes’ install options you will need to have the
[{remotes} package](https://github.com/r-lib/remotes) installed.

## Usage

``` r
library(checkdmarc)

# current version
packageVersion("checkdmarc")
## [1] '0.1.0'
```

``` r
library(psl)
library(stringi)
library(tibble)

dom <- stri_trans_tolower("fbi.gov")
apex <- apex_domain(dom)

(ns_recs <- get_nameservers(dom))
## # A tibble: 8 x 4
##   fqdn    nameserver                      ipv address              
##   <chr>   <chr>                         <int> <chr>                
## 1 fbi.gov ns-cloud-e1.googledomains.com     4 216.239.32.110       
## 2 fbi.gov ns-cloud-e1.googledomains.com     6 2001:4860:4802:32::6e
## 3 fbi.gov ns-cloud-e2.googledomains.com     4 216.239.34.110       
## 4 fbi.gov ns-cloud-e2.googledomains.com     6 2001:4860:4802:34::6e
## 5 fbi.gov ns-cloud-e3.googledomains.com     4 216.239.36.110       
## 6 fbi.gov ns-cloud-e3.googledomains.com     6 2001:4860:4802:36::6e
## 7 fbi.gov ns-cloud-e4.googledomains.com     4 216.239.38.110       
## 8 fbi.gov ns-cloud-e4.googledomains.com     6 2001:4860:4802:38::6e

(mx_recs <- get_mx_servers(dom, ns_recs$address))
## # A tibble: 1 x 3
##   fqdn    exchange         preference
## * <chr>   <chr>                 <int>
## 1 fbi.gov mx-east.fbi.gov.         10

(txt_recs <- get_txt_records(dom, ns_recs$address))
## # A tibble: 9 x 2
##   fqdn    txt                                                                                         
##   <fct>   <fct>                                                                                       
## 1 fbi.gov MS=ms39271050                                                                               
## 2 fbi.gov 625558384-8740534                                                                           
## 3 fbi.gov v=spf1 +mx ip4:153.31.0.0/16 -all                                                           
## 4 fbi.gov amazonses: iUbfpGEqhMPlcmJ0aykJZREltK6pWio9wOgRngnJOQE=                                     
## 5 fbi.gov google-site-verification=6UEk-jfg1xPNjz_rQGcRFJOBGxMy1aARDZUTXgSNAqw                        
## 6 fbi.gov google-site-verification=L8cauHJF4MANoTCkMbrLkAVfHBta28ctva9n1IDekTo                        
## 7 fbi.gov _globalsign-domain-verification=xZMJnzdDAgURaBjUZ6qbqWaaYmV5W3sfo3TF8mUxne                  
## 8 fbi.gov ublrZj1CzpSEiwtiRFKDAyiek8hRqkqaTTApxvhwai14i8JqVBOauW4cA06i39H5Lhl3HnALCM/xfTxIPEXEpA==    
## 9 fbi.gov adobe-idp-site-verification=101945e35b37c6efd526cf706f04bc9545a02f9cdc58dbf718678c506697d67d

(spf_recs <- get_spf_records(dom, ns_recs$address))
## # A tibble: 1 x 2
##   fqdn    txt                              
##   <fct>   <chr>                            
## 1 fbi.gov v=spf1 +mx ip4:153.31.0.0/16 -all

(dmarc_rec <- get_dmarc_record(dom))
## # A tibble: 1 x 2
##   fqdn          txt                                                                                                     
##   <fct>         <fct>                                                                                                   
## 1 _dmarc.fbi.g… v=DMARC1; p=reject; rua=mailto:dmarc-feedback@fbi.gov,mailto:reports@dmarc.cyber.dhs.gov; ruf=mailto:dm…

(get_bimi_record("ebay.com"))
## # A tibble: 1 x 2
##   fqdn                   txt                                                                                
##   <fct>                  <fct>                                                                              
## 1 default._bimi.ebay.com v=BIMI1; l=https://ir.ebaystatic.com/pictures/CollaborationInfra/ebay_logo_rgb.svg;

(get_ptr_records("24.61.106.4"))
## # A tibble: 1 x 4
##   ip            ipv ptr_name                  ptr                               
##   <chr>       <int> <chr>                     <chr>                             
## 1 24.61.106.4     4 4.106.61.24.in-addr.arpa. c-24-61-106-4.hsd1.nh.comcast.net.
```

``` r
(res <- check_starrtls(mx_recs$exchange[1]))
## [1] TRUE
```

## checkdmarc Metrics

| Lang | \# Files |  (%) | LoC |  (%) | Blank lines |  (%) | \# Lines |  (%) |
| :--- | -------: | ---: | --: | ---: | ----------: | ---: | -------: | ---: |
| R    |       13 | 0.93 | 207 | 0.91 |          85 | 0.77 |      100 | 0.75 |
| Rmd  |        1 | 0.07 |  21 | 0.09 |          25 | 0.23 |       33 | 0.25 |

## Code of Conduct

Please note that this project is released with a Contributor Code of
Conduct. By participating in this project you agree to abide by its
terms.
