is_modifier <- function(x) { stri_detect_fixed(x, "=") }
is_qualifier <- function(x) { stri_sub(x, 1, 1) %in% c("+", "-", "?", "~") }

start_modifier <- function(term) {

  list(
    term = term,
    key = NULL,
    value = NULL
  ) -> out

  class(out) <- c("qualifier", "list")
  out

}

start_directive <- function(term) {

  list(
    term = term,
    qualifier = NULL,
    mechanism = NULL,
    arguments = NULL
  ) -> out

  class(out) <- c("directive", "list")
  out

}

get_qualifier <- function(term) {
  if (is_qualifier(term)) stri_sub(term, 1, 1)
}

get_mechanism <- function(term) {
  if (is_qualifier(term)) term <- stri_sub(term, 2, nchar(term))
  stri_match_first_regex(term, "^([^:]+)")[,2]
}

.is_ipv4 <- function(x) {
  ifelse(is.na(is_ipv4(x)), FALSE, is_ipv4(x))
}

.is_ipv6 <- function(x) {
  ifelse(is.na(is_ipv6(x)), FALSE, is_ipv6(x))
}

.is_ip <- function(x) {
  .is_ipv4(x) | .is_ipv6(x)
}

get_arguments <- function(term) {

  args <- stri_match_first_regex(term, ":(.*)$")[,2]

  ## TODO finish parsing



}

get_modifier_keyval <- function(term) {
  set_names(as.list(unlist(stri_split_fixed(term, "=", 2))), c("key", "value"))
}

#' @export
validate_spf_record <- function(record, fqdn, already_seen = character(0), nameservers = checkdmarc_default_nameservers) {

  if (length(already_seen) == 0) already_seen <-  fqdn

  lookup_ways <- c("a", "mx", "include", "exists", "redirect")

  record <- stri_replace_all_regex(record, '"[[:space:]]+"', "")

  terms <- unlist(stri_split_regex(record, "[[:space:]]+"))

  # check version

  version_ok <- (terms[1] == "v=spf1")

  terms <- terms[-1]

  # get directives & modifiers

  modifiers <- list()
  directives <- list()

  for(term in terms) {

    if (is_modifier(term)) {
      modifiers <- append(modifiers, list(start_modifier(term)))
    } else {
      directives <- append(directives, list(start_directive(term)))
    }

  }

  lapply(directives, function(directive) {

    directive[["qualifier"]] <- get_qualifier(directive[["term"]])
    directive[["mechanism"]] <- get_mechanism(directive[["term"]])
    directive[["arguments"]] <- get_arguments(directive[["term"]])

    directive

  }) -> directives

  lapply(modifiers, function(modifier) {

    x <- get_modifier_keyval(modifier[["term"]])

    modifier[["key"]] <- x[["key"]]
    modifier[["value"]] <- x[["value"]]

    modifier

  }) -> modifiers

  list(
    version_ok = version_ok,
    modifiers = modifiers,
    directives = directives
  )

  # process directives
  # process modifiers

  # handle directives
  # handle modifiers

}

# validate_spf_record(
#   "rapid7.com",
#   "v=spf1 include:rapid7.com._nspf.vali.email include:%{i}._ip.%{h}._ehlo.%{d}._spf.vali.email ~all"
# ) %>%
#   str(3)
#
# validate_spf_record(
#   "fbi.gov",
#   "v=spf1 +mx ip4:153.31.0.0/16 -all"
# ) %>%
#   str(3)
#
# validate_spf_record(
#   "mxtoolbox.com",
#   "v=spf1 redirect=mxtoolbox.com.hosted.spf-report.com"
# ) %>%
#   str(3)
#
#
#
